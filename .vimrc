" colorscheme dracula
colorscheme gruvbox
" colorscheme molakai
" colorscheme nord
" colorscheme onedark

set autoindent
set background=dark
set cursorline
set colorcolumn=80
set encoding=utf-8
set expandtab
set fileformat=unix
set hidden
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set mouse=a
set nobackup
set nocompatible
set noswapfile
set number
set relativenumber
set ruler
set shiftwidth=4
set showcmd
set showmode
set smartcase
set softtabstop=4
set splitbelow 
set splitright
set tabstop=4
set visualbell

noremap <Right> <NOP>
noremap <Down> <NOP>
noremap <Up> <NOP>
noremap <Left> <NOP>

" Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Tagbar
nmap <F8> :TagbarToggle<CR>

" Vim Tmux Navigator
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> {Left-Mapping} :TmuxNavigateLeft<cr>
nnoremap <silent> {Down-Mapping} :TmuxNavigateDown<cr>
nnoremap <silent> {Up-Mapping} :TmuxNavigateUp<cr>
nnoremap <silent> {Right-Mapping} :TmuxNavigateRight<cr>
nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>
